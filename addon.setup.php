<?php

return array(
	'author'         => 'Andrew Weaver',
	'author_url'     => 'http://brandnewbox.co.uk/',
	'name'           => 'FormGrab Recaptcha',
	'description'    => 'Adds recaptcha v2 to FormGrab forms',
	'version'        => '1.0.0',
	'namespace'      => 'Brandnewbox\Formgrab_recaptcha',
	'settings_exist' => TRUE
);