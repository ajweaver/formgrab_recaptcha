<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package     ExpressionEngine
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2003 - 2018, EllisLab, Inc.
 * @license     http://expressionengine.com/user_guide/license.html
 * @link        http://expressionengine.com
 * @since       Version 2.0
 * @filesource
 */

/**
 * FormGrab Recaptcha Extension
 *
 * @package    ExpressionEngine
 * @subpackage Addons
 * @category   Extension
 * @author     Andrew Weaver
 * @link       http://brandnewbox.co.uk/
 */
class Formgrab_recaptcha_ext
{
    public $settings       = array();
    public $description    = 'Adds recaptcha v2 to FormGrab forms';
    public $docs_url       = '';
    public $name           = 'FormGrab Recaptcha';
    public $settings_exist = 'y';
    public $version        = '1.0.0';

    /**
     * Constructor
     *
     * @param   mixed Settings array or empty string if none exist.
     */
    public function __construct($settings = '')
    {
        $this->settings = $settings;
    }

    /**
     * Activate Extension
     *
     * This function enters the extension into the exp_extensions table
     *
     * @see http://codeigniter.com/user_guide/database/index.html for
     * more information on the db class.
     *
     * @return void
     */
    public function activate_extension()
    {
        // Setup custom settings in this array.
        $this->settings = array();

        ee()->db->insert_batch('extensions', array(
            array(
                'class' => __CLASS__,
                'method' => 'formgrab_form_tagdata',
                'hook' => 'formgrab_form_tagdata',
                'settings' => serialize($this->settings),
                'version' => $this->version,
                'enabled' => 'y',
            ),
            array(
                'class' => __CLASS__,
                'method' => 'formgrab_validate',
                'hook' => 'formgrab_validate',
                'settings' => serialize($this->settings),
                'version' => $this->version,
                'enabled' => 'y',
            ),
            array(
                'class' => __CLASS__,
                'method' => 'formgrab_save_submission',
                'hook' => 'formgrab_save_submission',
                'settings' => serialize($this->settings),
                'version' => $this->version,
                'enabled' => 'y',
            ),
        ));
    }

    function settings()
    {
        $settings = array();

        // General pattern:
        //
        // $settings[variable_name] => array(type, options, default);
        //
        // variable_name: short name for the setting and the key for the language file variable
        // type:          i - text input, t - textarea, r - radio buttons, c - checkboxes, s - select, ms - multiselect
        // options:       can be string (i, t) or array (r, c, s, ms)
        // default:       array member, array of members, string, nothing

        $settings['site_key']      = array('i', '', "");
        $settings['secret_key']      = array('i', '', "");

        return $settings;
    }

    /**
     * formgrab_form_tagdata Hook
     *
     * @param
     * @return
     */
    public function formgrab_form_tagdata( $tagdata )
    {
        $tagdata = ee()->extensions->last_call ? ee()->extensions->last_call : $tagdata;

        // todo: ensure settings exist first
        // todo: make this more robust for multiple forms on page
        // Look for {formgrab_recaptcha} tag in form
        if( strpos($tagdata, '{formgrab_recaptcha}' ) ) {
            $tagdata = str_replace('{formgrab_recaptcha}', '<div class="g-recaptcha" data-sitekey="' . $this->settings['site_key'] . '"></div>', $tagdata);
            $tagdata .= '<script src="https://www.google.com/recaptcha/api.js" async defer></script>';
        }

        return $tagdata;
    }

    /**
     * formgrab_validate Hook
     *
     * @param
     * @return
     */
    public function formgrab_validate( $post )
    {
        $post = ee()->extensions->last_call ? ee()->extensions->last_call : $post;

        // todo: !!!
        if( isset( $post['g-recaptcha-response'] ) ) {

            // Build google verification request
            $post_data = http_build_query(
                array(
                    'secret' => $this->settings['secret_key'],
                    'response' => $post['g-recaptcha-response'],
                    'remoteip' => ee()->input->ip_address()
                )
            );
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $post_data
                )
            );
            $context  = stream_context_create($opts);
            $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
            $result = json_decode($response);
            if( empty($result) || !$result->success) {
                $error = array();
                $error[] = ee()->lang->line('Recaptcha failed. Please try again.');
                return ee()->output->show_user_error('general', $error);
            }

            // Remove the google recaptch field from the list of fields to save
            unset( $post['g-recaptcha-response'] );

        }

        return $post;
    }

    /**
     * formgrab_save_submission Hook
     *
     * @param
     * @return
     */
    public function formgrab_save_submission( $submission, $post )
    {
        return $submission;
    }

    /**
     * Disable Extension
     *
     * This method removes information from the exp_extensions table
     *
     * @return void
     */
    public function disable_extension()
    {
        ee()->db->delete('extensions', array('class' => __CLASS__));
    }

    /**
     * Update Extension
     *
     * This function performs any necessary db updates when the extension
     * page is visited
     *
     * @return  mixed void on update / false if none
     */
    public function update_extension($current = '')
    {
        if ($current == '' OR $current == $this->version)
        {
            return FALSE;
        }

        ee()->db->update('extensions', array('version' => $this->version), array('class' => __CLASS__));
    }
}

/* End of file ext.formgrab_recaptcha.php */
/* Location: /system/expressionengine/third_party/formgrab_recaptcha/ext.formgrab_recaptcha.php */