# FormGrab ReCaptcha

## Introduction

This provides basic support for adding Google's [ReCaptcha](http://www.google.com/recaptcha/) v2 system to [FormGrab](https://brandnewbox.co.uk/products/details/formgrab) forms.

## Installation

Copy the `formgrab_recaptcha` folder into your `system/user/addons` folder and install it from the ExpressionEngine Control Panel Add-ons page.

You must provide a Google ReCaptcha **Site Key** and **Secret Key** in the add-ons settings page. These can be obtained from [Google](http://www.google.com/recaptcha/admin)

## Usage

### Creating a form

Create a form with FormGrab as usual but include the variable:

	{formgrab_recaptcha}

in the form, eg:

	{exp:formgrab:form
		name="recaptcha_form"
		title="Recaptcha Form"
	}

	  <div class="form-group">
	    <label for="name">Name</label>
	    <input name="name" type="text" class="form-control" id="name" placeholder="Your name">
	  </div>
	  <div class="form-group">
	    <label for="email">Email address</label>
	    <input name="email" type="email" class="form-control" id="email" placeholder="Your email address">
	  </div>
	  <div class="form-group">
	    <label for="message">Message</label>
	    <textarea name="message" class="form-control" id="message" rows="3"></textarea>
	  </div>

	<p>{formgrab_recaptcha}</p>

	<button type="submit" class="btn btn-primary">Submit</button>

	</div>

	{/exp:formgrab:form}

## Limitations

2019-01-12

Currently this add-on will only check that the recaptcha is valid *if* it finds the recaptcha field in the submitted form (so if the user removes the recaptcha it will not stop the spam submission). This limitation will be removed when an update to FormGrab is released.

## Support

The recommended way to get help is to email <a href="mailto:support@brandnewbox.co.uk">support@brandnewbox.co.uk</a>.
