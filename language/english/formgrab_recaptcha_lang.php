<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(

	'site_key' => 'Site key (<a target="_popup" href="http://www.google.com/recaptcha/admin">Get API Key from google</a>)',
	'secret_key' => 'Secret key',

);

/* End of file formgrab_recaptcha_lang.php */
/* Location: /system/expressionengine/third_party/formgrab_recaptcha/language/english/formgrab_recaptcha_lang.php */